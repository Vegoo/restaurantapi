# Simple Restaurant API
Simple CRUD application created with ASP.NET Core 5.0
# Entities
- Restaurant
- Dish
- Address
- User
- Role
# API endpoints
![](docs/Restaurant.png)
