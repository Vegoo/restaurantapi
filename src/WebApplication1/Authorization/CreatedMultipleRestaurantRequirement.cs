﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Restaurants.Authorization
{
    public class CreatedMultipleRestaurantRequirement : IAuthorizationRequirement
    {
        public int MinimumRestaurantCreated { get; set; }

        public CreatedMultipleRestaurantRequirement(int minimumRestaurantCreated)
        {
            MinimumRestaurantCreated = minimumRestaurantCreated;
        }
    }
}
