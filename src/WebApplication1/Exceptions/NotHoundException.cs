﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Restaurants.Exceptions
{
    public class NotHoundException : Exception
    {
        public NotHoundException(string message) : base(message)
        {

        }
    }
}
