﻿namespace Restaurants.Models
{
    public enum SortDirection
    {
        ASC,
        DESC
    }
}
